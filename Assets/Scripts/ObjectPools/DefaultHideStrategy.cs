﻿using UnityEngine;

namespace ObjectPools
{
    public class DefaultHideStrategy : IHideStrategy
    {
        public void Hide(GameObject objectToHide)
        {
            objectToHide.SetActive(false);
        }
    }
}