﻿using UnityEngine;

public interface IHideStrategy
{
    void Hide(GameObject objectToHide);
}
