﻿using System.Collections.Generic;
using UnityEngine;

namespace ObjectPools
{
    public class ObjectPool<T> where T : MonoBehaviour, IGameObjectPooled<T>
    {
        public int PoolSize => _items.Count;
        
        private readonly T _prefab;
        private readonly Transform _poolParent;
        private readonly IHideStrategy _hideStrategy;
        
        private PoolManager<T> _poolManager;
        
        private readonly Queue<T> _items = new Queue<T>();
        
        public ObjectPool(T prefab, Transform poolParent, IHideStrategy hideStrategy)
        {
            _prefab = prefab;
            _poolParent = poolParent;
            _hideStrategy = hideStrategy;
        }

        public void SetPoolManager(PoolManager<T> poolManager)
        {
            _poolManager = poolManager;
        }

        public void AddObject()
        {
            var obj = Object.Instantiate(_prefab, _poolParent);
            _hideStrategy.Hide(obj.gameObject);

            obj.PoolManager = _poolManager;

            _items.Enqueue(obj);
        }

        public T GetObject()
        {
            if (_items.Count == 0)
            {
                AddObject();
            }

            return _items.Dequeue();
        }

        public void ReturnToPool(T poolableObject)
        {
            _hideStrategy.Hide(poolableObject.gameObject);

            _items.Enqueue(poolableObject);
        }
    }
}