﻿using UnityEngine;

namespace ObjectPools
{
    public interface IGameObjectPooled<T> where T : MonoBehaviour, IGameObjectPooled<T>
    {
        PoolManager<T> PoolManager { get; set; }

        void Release();
    }
}
