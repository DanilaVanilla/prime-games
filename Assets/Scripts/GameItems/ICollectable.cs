﻿namespace GameItems
{
    public interface ICollectable
    {
        void Collect();
    }
}