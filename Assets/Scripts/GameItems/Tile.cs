﻿using ObjectPools;
using UnityEngine;

namespace GameItems
{
    [RequireComponent(typeof(MeshFilter))]
    public class Tile : MonoBehaviour, IGameObjectPooled<Tile>
    {
#pragma warning disable 649
        [SerializeField] private MeshFilter _meshFilter;
#pragma warning restore 649
        public PoolManager<Tile> PoolManager { get; set; }
        
        public float TileLength { get; private set; }
    
        private void Awake()
        {
            var sharedMesh = _meshFilter.sharedMesh;
            TileLength = sharedMesh.bounds.size.z * transform.localScale.z;
        }
        
        private void OnBecameInvisible()
        {
            Release();
        }
        
        public void Release()
        {
            PoolManager.ReturnToPool(this);
        }
    }
}