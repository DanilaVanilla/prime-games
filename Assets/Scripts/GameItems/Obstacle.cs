﻿namespace GameItems
{
    public class Obstacle : LaneObject
    {    
        protected override void CalculateHeight()
        {
            var sharedMesh = MeshFilter.sharedMesh;
            Height = sharedMesh.bounds.size.y * transform.localScale.y * 0.5f;
        }
    }
}