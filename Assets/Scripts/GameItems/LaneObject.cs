﻿using ObjectPools;
using UnityEngine;

namespace GameItems
{
    public abstract class LaneObject : MonoBehaviour, IGameObjectPooled<LaneObject>
    {        
        public PoolManager<LaneObject> PoolManager{ get; set; }
        
        public float Height { get; protected set; }
        
        [SerializeField] protected MeshFilter MeshFilter;
        
        protected abstract void CalculateHeight();
        
        private void Awake()
        {
            CalculateHeight();
        }
        
        private void OnBecameInvisible()
        {
            Release();
        }

        public void Release()
        {
           PoolManager.ReturnToPool(this);
        }
    }
}