﻿using UnityEngine;

namespace GameItems
{
    public class Coin : LaneObject, ICollectable
    {
        [SerializeField] private float _rotationSpeed = 10f;

        protected override void CalculateHeight()
        {
            var sharedMesh = MeshFilter.sharedMesh;
            Height = sharedMesh.bounds.size.x * transform.localScale.x * 0.5f;
        }

        public void Collect()
        {
            Release();
        }

        private void Update()
        {
            Rotate();
        }

        private void Rotate()
        {
            transform.Rotate(0, _rotationSpeed * Time.deltaTime, 0, Space.World);
        }
    }
}