﻿using System;
using UnityEngine;

namespace Currencies
{
    public class Money
    {
        private const string MoneyAmountKey = "MONEY_AMOUNT_KEY";

        public int Amount
        {
            get
            {
                return PlayerPrefs.GetInt(MoneyAmountKey, 0);
            }

            private set
            {
                PlayerPrefs.SetInt(MoneyAmountKey, value);
                PlayerPrefs.Save();
            }
        }

        public void AddCoin()
        {
            Amount++;
        }
    }
}