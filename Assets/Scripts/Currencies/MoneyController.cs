﻿using Currencies;
using UI;

public class MoneyController
{
    private readonly Money _money;
    private readonly MoneyView _moneyView;

    private readonly Player _player;
    
    public MoneyController(Money money, MoneyView moneyView, Player player)
    {
        _money = money;
        _moneyView = moneyView;
        _player = player;
        
        _moneyView.Init(_money.Amount);
        
        _player.OnCollectCoin += OnOnCollectCoin;
    }

    ~MoneyController()
    {
        _player.OnCollectCoin -= OnOnCollectCoin;
    }

    private void OnOnCollectCoin()
    {
        _money.AddCoin();
        _moneyView.UpdateMoneyAmountView(_money.Amount);
    }
}