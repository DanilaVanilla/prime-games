﻿using System;
using Enums;

namespace Utils
{
    public static class LaneExtension
    {
        public static Lane MoveLeft(this Lane lane)
        {
            if (Enum.IsDefined(typeof(Lane), (int) lane - 1))
            {
                return lane - 1;
            }

            return lane;
        }

        public static Lane MoveRight(this Lane lane)
        {
            if (Enum.IsDefined(typeof(Lane), (int) lane + 1))
            {
                return lane + 1;
            }

            return lane;
        }

        public static float GetX(this Lane lane, float laneWidth)
        {
            return (int) lane * laneWidth;
        }

        public static Lane GetRandomLane()
        {
            var values = Enum.GetValues(typeof(Lane));
            var randomLane = (Lane) values.GetValue(UnityEngine.Random.Range(0, values.Length));
        
            return randomLane;
        }
    
        public static int GetAmountOfLanes()
        {
            var values = Enum.GetValues(typeof(Lane));
        
            return values.Length;
        }
    }
}