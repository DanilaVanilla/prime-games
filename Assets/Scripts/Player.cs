﻿using System;
using DG.Tweening;
using Enums;
using GameItems;
using Ui;
using UI;
using UnityEngine;
using Utils;

public class Player : MonoBehaviour
{
    [SerializeField] private float _startSpeed = 1.0f;
    [SerializeField] private float _speedIncrementByCoin = 0.25f;
    [SerializeField] private float _laneWidth = 4.0f;

    private DistanceView _distanceView;

    private WindowsManager _windowsManager;

    private float _currentSpeed;

    private Lane _currentLane;

    private Vector3 _direction;

    private bool _isAlive;
    private bool _isStarted;

    public event Action OnCollectCoin;

    public void Init(WindowsManager windowsManager, DistanceView distanceView)
    {
        _windowsManager = windowsManager;
        _distanceView = distanceView;
        
        _isAlive = true;
        _isStarted = false;

        _currentLane = Lane.Middle;
        _currentSpeed = _startSpeed;

        _direction = Vector3.forward;
    }

    private void Update()
    {
        HandleInput();
        Move();
    }

    private void HandleInput()
    {
        if (!_isAlive) return;

        if (Input.GetKeyDown(KeyCode.Space) && !_isStarted)
        {
            _isStarted = true;
        }

        if (!_isStarted) return;

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            _currentLane = _currentLane.MoveLeft();

            ChangeLane();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _currentLane = _currentLane.MoveRight();

            ChangeLane();
        }
    }

    private void ChangeLane()
    {
        transform.DOMoveX(_currentLane.GetX(_laneWidth), 0.5f);
    }

    private void Move()
    {
        if (!_isAlive || !_isStarted) return;

        transform.Translate(_direction * _currentSpeed * Time.deltaTime);

        UpdateDistanceView();
    }

    private void UpdateDistanceView()
    {
        _distanceView.SetDistanceText(transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Coin>(out var coin))
        {
            OnCollectCoin?.Invoke();
            
            _currentSpeed += _speedIncrementByCoin;

            coin.Collect();
        }
        else if (other.GetComponent<Obstacle>())
        {
            Dead();
        }
    }

    private void Dead()
    {
        if (!_isAlive) return;

        _isAlive = false;

        PlayDeadAnimation();
    }

    private void PlayDeadAnimation()
    {
        DOTween.KillAll();

        const float jumpForce = 0.5f;
        const int jumpNum = 1;
        const float jumpDuration = 0.5f;

        var sequence = transform.DOJump(transform.position, jumpForce, jumpNum, jumpDuration);

        var rotationAngles = new Vector3(0, 0, 90);
        const float rotationDuration = 0.25f;

        sequence.Append(transform.DORotate(rotationAngles, rotationDuration));

        const float positionToMoveY = 0.5f;
        const float moveDurationY = 0.25f;

        sequence.Join(transform.DOMoveY(positionToMoveY, moveDurationY)).OnComplete(() =>
        {
            _windowsManager.Show<GameOverWindow>();
        });
    }
}