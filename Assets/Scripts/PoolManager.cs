﻿using System;
using ObjectPools;
using UnityEngine;
using Object = UnityEngine.Object;

public class PoolManager<T> where T : MonoBehaviour, IGameObjectPooled<T>
{
    private readonly int _startedLength;
    private readonly int _maximumLength;

    private readonly ObjectPool<T> _objectPool;

    public PoolManager(int startedLength, int maximumLength, ObjectPool<T> objectPool)
    {
        if (startedLength > maximumLength)
        {
            throw new ArgumentOutOfRangeException(nameof(startedLength),
                "Started Length can not be bugger than Maximum Length");
        }

        _startedLength = startedLength;
        _maximumLength = maximumLength;

        _objectPool = objectPool;
        _objectPool.SetPoolManager(this);

        CreateObjects();
    }

    private void CreateObjects()
    {
        for (var i = 0; i < _startedLength; i++)
        {
            _objectPool.AddObject();
        }
    }

    public T GetObject()
    {
        return _objectPool.GetObject();
    }

    public void ReturnToPool(T objectPooled)
    {
        if (_objectPool.PoolSize <= _maximumLength)
        {
            _objectPool.ReturnToPool(objectPooled);
        }
        else
        {
            Object.Destroy(objectPooled);
        }
    }
}