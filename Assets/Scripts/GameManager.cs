﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Currencies;
using GameItems;
using ObjectPools;
using Spawners;
using Ui;
using UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{
#pragma warning disable 649
    [Header("Player")] [SerializeField] private Player _player;

    [Header("User Interface Prefabs")] 
    [SerializeField] private Window[] _windowPrefabs;
    [SerializeField] private GameInterface _gameInterfacePrefab;

    [Header("Game Prefabs")] 
    [SerializeField] private Tile _tilePrefab;
    [SerializeField] private LaneObject _coinPrefab;
    [SerializeField] private LaneObject _obstaclePrefab;

    [Header("Pools Parents")]
    [SerializeField] private Transform _tilePoolParent;
    [SerializeField] private Transform _coinPoolParent;
    [SerializeField] private Transform _obstaclePoolParent;

    [Header("Pool Settings")] 
    [SerializeField] private int _poolStartedLength = 25;
    [SerializeField] private int _maxPoolSize = 50;

    [Header("Spawners")] 
    [SerializeField] private TileSpawner _tileSpawner;
    [SerializeField] private LaneObjectSpawner _coinSpawner;
    [SerializeField] private LaneObjectSpawner _obstacleSpawner;

    [Header("Parent Points")] 
    [SerializeField] private Transform _userInterfaceParentPoint;
#pragma warning restore 649

    private GameInterface _gameInterface;
    
    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        var windowsManager = new WindowsManager();
        var gameInterface = CreateGameInterface();
        var windows = CreateWindows(_windowPrefabs);
        
        InitializePlayer(windowsManager, gameInterface.GetDistanceView());
        InitializeCurrency(gameInterface.GetMoneyView());
        InitializeWindows(windowsManager, windows);

        InitializeSpawners();
    }

    private GameInterface CreateGameInterface()
    {
        return Instantiate(_gameInterfacePrefab, _userInterfaceParentPoint);
    }

    private void InitializePlayer(WindowsManager windowsManager, DistanceView distanceView)
    {
        _player.Init(windowsManager, distanceView);
    }

    private void InitializeCurrency(MoneyView moneyView)
    {
        var money = new Money();
        var moneyController = new MoneyController(money, moneyView, _player);
    }

    private IEnumerable<Window> CreateWindows(Window[] windowPrefabs)
    {
        return _windowPrefabs.Select(window => Instantiate(window, _userInterfaceParentPoint)).ToList();
    }

    private void InitializeWindows(WindowsManager windowsManager, IEnumerable<Window> windows)
    {
        foreach (var window in windows)
        {
            windowsManager.Register(window);
        }
    }

    private void InitializeSpawners()
    {
        var hideStrategy = new DefaultHideStrategy();
        
        var tilePool = new ObjectPool<Tile>(_tilePrefab, _tilePoolParent, hideStrategy);
        var tilePoolManager = new PoolManager<Tile>(_poolStartedLength, _maxPoolSize, tilePool);
        
        _tileSpawner.Init(_player.transform, tilePoolManager);
        
        var coinsPool = new ObjectPool<LaneObject>(_coinPrefab, _coinPoolParent, hideStrategy);
        var coinsPoolManager = new PoolManager<LaneObject>(_poolStartedLength, _maxPoolSize, coinsPool);
        
        _coinSpawner.Init(_player.transform, coinsPoolManager);
        
        var obstaclePool = new ObjectPool<LaneObject>(_obstaclePrefab, _obstaclePoolParent, hideStrategy);
        var obstaclePoolManager = new PoolManager<LaneObject>(_poolStartedLength, _maxPoolSize, obstaclePool);
        
        _obstacleSpawner.Init(_player.transform, obstaclePoolManager);
    }
}