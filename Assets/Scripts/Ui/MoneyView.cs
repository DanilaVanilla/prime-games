﻿using Currencies;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MoneyView : MonoBehaviour
    {        
#pragma warning disable 649
        [SerializeField] private Text _moneyText;
#pragma warning restore 649
        
        public void Init(int startAmount)
        {
            SetMoneyText(startAmount);
        }

        public void UpdateMoneyAmountView(int amount)
        {
            SetMoneyText(amount);
            UpdateMoneyAnim();
        }

        private void UpdateMoneyAnim()
        {
            const float animDuration = 0.1f;

            _moneyText.transform.DOPunchScale(Vector3.one, animDuration).OnComplete(() =>
            {
                _moneyText.transform.localScale = Vector3.one;
            });
        }

        private void SetMoneyText(int moneyAmount)
        {
            _moneyText.text = moneyAmount.ToString("00");
        }
    }
}