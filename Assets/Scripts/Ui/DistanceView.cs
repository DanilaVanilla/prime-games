﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class DistanceView : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private Text _distanceText;
#pragma warning restore 649
        private const string DistanceStringFormat = "{0:00} m.";

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            SetDistanceText(0.0f);
        }

        public void SetDistanceText(float distance)
        {
            _distanceText.text = string.Format(DistanceStringFormat, distance);
        }
    }
}