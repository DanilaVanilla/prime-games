﻿using System.Collections.Generic;

namespace Ui
{
    public class WindowsManager
    {
        private readonly List<IGameUi> _objects = new List<IGameUi>();
        
        public void Register<T>(T window) where T : IGameUi
        {
           _objects.Add(window);
        }

        public T Show<T>() where T : IGameUi
        {
            var obj = Get<T>();
            if (obj == null) return default(T);

            obj.Show();

            return obj;
        }
        
        private T Get<T>() where T : IGameUi
        {
            foreach (var window in _objects)
            {
                if (window is T variable) return variable;
            }

            return default (T);
        }
    }
}