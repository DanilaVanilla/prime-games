﻿using UI;
using UnityEngine;

namespace Ui
{
    public class GameInterface : MonoBehaviour
    {
        [SerializeField] private MoneyView _moneyView;
        [SerializeField] private DistanceView _distanceView;

        public MoneyView GetMoneyView()
        {
            return _moneyView;
        }

        public DistanceView GetDistanceView()
        {
            return _distanceView;
        }
    }
}
