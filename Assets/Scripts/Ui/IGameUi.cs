﻿namespace Ui
{
    public interface IGameUi
    {
        void Show();
        void Hide();
    }
}