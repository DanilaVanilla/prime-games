﻿using UnityEngine;

namespace Ui
{
    public abstract class Window : MonoBehaviour, IGameUi
    {
        public abstract void Show();
        public abstract void Hide();
    }
}