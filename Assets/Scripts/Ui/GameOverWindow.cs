﻿using System.Collections;
using Ui;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class GameOverWindow : Window
    {
#pragma warning disable 649
        [SerializeField] private Button _restartGameButton;
#pragma warning restore 649
        private void Awake()
        {
            _restartGameButton.onClick.AddListener(RestartGameButton);
        }

        private void RestartGameButton()
        {
            RestartCurrentScene();
        }

        private void RestartCurrentScene()
        {
            StartCoroutine(LoadYourAsyncScene());
        }

        private IEnumerator LoadYourAsyncScene()
        {
            var asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            Hide();
        }

        public override void Show()
        {
            gameObject.SetActive(true);
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}