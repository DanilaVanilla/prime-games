﻿using GameItems;
using ObjectPools;
using UnityEngine;
using Utils;

namespace Spawners
{
    public class LaneObjectSpawner : BaseSpawner
    {
        private PoolManager<LaneObject> _pool;

        [SerializeField] private float _distanceBetweenSpawnItems = 10f;
        [SerializeField] private float _laneLength = 3.0f;
        
        public void Init(Transform player, PoolManager<LaneObject> laneObjectPool)
        {
            _pool = laneObjectPool;
            
            Init(player);
        }
        
        protected override void Spawn()
        {
            var laneObject = _pool.GetObject();
            laneObject.gameObject.SetActive(true);

            var randomLane = LaneExtension.GetRandomLane();
            var randomPosition = new Vector3(randomLane.GetX(_laneLength), laneObject.Height,
                _distanceBetweenSpawnItems + LastItemPosition.z);

            laneObject.transform.position = randomPosition;
            LastItemPosition = randomPosition;
        }
    }
}