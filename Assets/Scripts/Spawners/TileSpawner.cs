﻿using GameItems;
using ObjectPools;
using UnityEngine;

namespace Spawners
{
    public class TileSpawner : BaseSpawner
    {   
        private PoolManager<Tile> _tilePool;

        public void Init(Transform player, PoolManager<Tile> tilePool)
        {
            _tilePool = tilePool;
            
            Init(player);
        }

        protected override void SpawnFirstItemsInternal()
        {
            var tile = _tilePool.GetObject();
            tile.gameObject.SetActive(true);

            LastItemPosition = transform.position;
        }

        protected override void Spawn()
        {
            var tile = _tilePool.GetObject();
            
            tile.gameObject.SetActive(true);
            tile.transform.position = LastItemPosition + new Vector3(0, 0, tile.TileLength);

            LastItemPosition = tile.transform.position;
        }
    }
}