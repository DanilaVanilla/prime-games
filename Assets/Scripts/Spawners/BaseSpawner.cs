﻿using UnityEngine;

namespace Spawners
{
    public abstract class BaseSpawner : MonoBehaviour
    {
        [SerializeField] private float _distanceThreshold = 100.0f;

        [SerializeField] private int _startedAmount = 10;

        private Transform _player;
        protected Vector3 LastItemPosition;
       
        protected abstract void Spawn();
           
        private void Update()
        {
            if (Vector3.Distance(_player.position, LastItemPosition) <= _distanceThreshold)
            {
                Spawn();
            }
        }
        
        public void Init(Transform player)
        {
            _player = player;
            LastItemPosition = Vector3.zero;

            SpawnFirstItems();
        }
     
        protected virtual void SpawnFirstItems()
        {
            SpawnFirstItemsInternal();
            
            for (var i = 0; i < _startedAmount; i++)
            {
                Spawn();
            }
        }

        protected virtual void SpawnFirstItemsInternal(){}
    }
}
